plugins {
    kotlin("jvm") version "1.9.0"
    kotlin("plugin.allopen") version "1.9.0"
    `java-gradle-plugin`
    id("com.gradle.plugin-publish") version "1.2.1"
    `maven-publish`
    jacoco
}

val tag: String? = System.getenv("CI_COMMIT_TAG")
val snapshotVersion = "0.3.3-SNAPSHOT"

group = "factory.tools.gradle"
version = if (!tag.isNullOrBlank() && tag.startsWith("v")) tag.substring(1) else snapshotVersion

val kotlinVersion = "1.9.0"

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

configurations {
    configureEach {
        attributes {
            attribute(TargetJvmVersion.TARGET_JVM_VERSION_ATTRIBUTE, 8)
        }
    }
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("reflect", kotlinVersion))
    implementation("de.undercouch:gradle-download-task:5.5.0")
    testImplementation("io.kotest:kotest-runner-junit5-jvm:5.7.2")
    testImplementation("io.kotest:kotest-property-jvm:5.7.2")
}

allOpen {
    annotation("org.gradle.api.tasks.CacheableTask")
}

@Suppress("UnstableApiUsage")
gradlePlugin {
    website.set("https://gitlab.com/factory-org/tools/gradle-go")
    vcsUrl.set("https://gitlab.com/factory-org/tools/gradle-go")
    plugins {
        create("go") {
            id = "factory.tools.gradle.go"
            displayName = "Go Language Plugin"
            description = "A plugin that allows you to manage go projects"
            implementationClass = "factory.tools.gradle.go.GoPlugin"
            tags.set(listOf("go", "golang", "language"))
        }
    }
}

publishing {
    publications {
        withType<MavenPublication> {
            pom {
                licenses {
                    license {
                        name.set("The Apache License, Version 2.0")
                        url.set("https://www.apache.org/licenses/LICENSE-2.0.txt")
                    }
                }
                developers {
                    developer {
                        id.set("cromefire_")
                        name.set("Cromefire_")
                        email.set("cromefire_@outlook.com")
                    }
                }
            }
        }
    }
    repositories {
        maven("https://gitlab.com/api/v4/projects/19214350/packages/maven") {
            name = "GitLab"

            credentials(HttpHeaderCredentials::class.java) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN") ?: ""
            }
            authentication {
                register("header", HttpHeaderAuthentication::class.java)
            }
        }
    }
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }

    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }

    withType<Test> {
        useJUnitPlatform()
    }

    test {
        reports.junitXml.required.set(true)

        finalizedBy(jacocoTestReport) // report is always generated after tests run
    }

    jacocoTestReport {
        reports {
            xml.required.set(true)
        }

        dependsOn(test) // tests are required to run before generating the report
    }
}
