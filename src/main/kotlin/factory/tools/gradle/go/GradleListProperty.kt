package factory.tools.gradle.go

import org.gradle.api.*
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.Provider
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

internal open class GradleListProperty<V> : ReadWriteProperty<Any, List<V>> {
    val displayName: String

    private val property: ListProperty<V>

    constructor(project: Project, displayName: String, type: Class<V>, default: List<V>? = listOf()) {
        this.displayName = displayName
        this.property = project.objects.listProperty(type).apply {
            set(default)
        }
    }

    constructor(project: Project, displayName: String, type: Class<V>, default: Provider<List<V>>) {
        this.displayName = displayName
        this.property = project.objects.listProperty(type).apply {
            set(default)
        }
    }

    override operator fun getValue(thisRef: Any, property: KProperty<*>): List<V> =
        this.property.orNull ?: throw InvalidUserDataException("$displayName has to be initialized")

    override operator fun setValue(thisRef: Any, property: KProperty<*>, value: List<V>) =
        this.property.set(value)
}
