package factory.tools.gradle.go

import java.io.Serializable

data class GoTarget(val os: String, val arch: String) : Serializable {
    companion object {
        private const val serialVersionUID: Long = 1
    }
}
