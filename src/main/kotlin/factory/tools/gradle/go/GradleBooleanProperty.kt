package factory.tools.gradle.go

import org.gradle.api.*
import org.gradle.api.provider.Property
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

@Suppress("PLATFORM_CLASS_MAPPED_TO_KOTLIN")
internal class GradleBooleanProperty(
    project: Project,
    val displayName: String,
    default: Boolean? = null
) : ReadWriteProperty<Any, Boolean> {
    private val property: Property<java.lang.Boolean> = project.objects.property(java.lang.Boolean::class.java).apply {
        set(default as? java.lang.Boolean)
    }

    override operator fun getValue(thisRef: Any, property: KProperty<*>): Boolean =
        (this.property.orNull ?: throw InvalidUserDataException("$displayName has to be initialized")).booleanValue()

    override operator fun setValue(thisRef: Any, property: KProperty<*>, value: Boolean) =
        this.property.set(value as? java.lang.Boolean)
}
