package factory.tools.gradle.go

import org.gradle.api.*

internal fun validateUserData(expr: Boolean, message: String) {
    if (!expr) {
        throw InvalidUserDataException(message)
    }
}
