package factory.tools.gradle.go

import org.gradle.internal.os.OperatingSystem

internal fun goArchSlug(): String {
    val os = OperatingSystem.current()

    return when (os.nativePrefix) {
        "linux-amd64" -> "linux-amd64"
        else -> throw IllegalStateException(
            "${os.name} is currently unsupported, please file an issue mentioning this string \"${os.nativePrefix}\" (if go is supported on your platform)"
        )
    }
}

internal data class GoDownloadInfo(
    val version: String,
    val slug: String,
    val ext: String
) {
    val fileName: String
        get() = "go$version.$slug.$ext"

    val url: String
        get() = "https://dl.google.com/go/$fileName"

    companion object {
        fun build(version: String): GoDownloadInfo {
            val os = OperatingSystem.current()
            val slug = goArchSlug()

            val ext = when {
                os.isLinux -> "tar.gz"
                else -> throw IllegalStateException("${os.name} is currently unsupported")
            }

            return GoDownloadInfo(
                version, slug, ext
            )
        }
    }
}
