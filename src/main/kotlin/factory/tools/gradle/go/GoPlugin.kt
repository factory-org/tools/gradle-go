package factory.tools.gradle.go

import de.undercouch.gradle.tasks.download.DownloadTaskPlugin
import org.gradle.api.*

internal const val GO_EXTENSION_NAME = "golang"

class GoPlugin : Plugin<Project> {
    override fun apply(project: Project) {
        createGoDsl(project)

        project.plugins.apply(DownloadTaskPlugin::class.java)
    }
}

internal val Project.golang: GoExtension
    get() =
        extensions.getByName(GO_EXTENSION_NAME) as? GoExtension
            ?: throw IllegalStateException("$GO_EXTENSION_NAME is not of the correct type")
