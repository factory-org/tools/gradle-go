package factory.tools.gradle.go

import org.gradle.api.*
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

internal open class GradleProperty<V> : ReadWriteProperty<Any, V> {
    val displayName: String

    private val property: Property<V>

    constructor(project: Project, displayName: String, type: Class<V>, default: V? = null) {
        this.displayName = displayName
        this.property = project.objects.property(type).apply {
            set(default)
        }
    }

    constructor(project: Project, displayName: String, type: Class<V>, default: Provider<V>) {
        this.displayName = displayName
        this.property = project.objects.property(type).apply {
            set(default)
        }
    }

    override operator fun getValue(thisRef: Any, property: KProperty<*>): V =
        this.property.orNull ?: throw InvalidUserDataException("$displayName has to be initialized")

    override operator fun setValue(thisRef: Any, property: KProperty<*>, value: V) =
        this.property.set(value)
}
