package factory.tools.gradle.go

import org.gradle.api.*

open class GoExtension(project: Project) {
    // Basic
    var version by GradleProperty(project, "Go version", String::class.java)

    // Advanced
    var installInRoot by GradleBooleanProperty(project, "Go setting 'installInRoot'", true)

    var defaultBuild by GradleBooleanProperty(project, "Go setting 'defaultBuild'", true)
}
