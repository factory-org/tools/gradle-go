package factory.tools.gradle.go.tasks

import factory.tools.gradle.go.GradleProperty
import org.gradle.api.*
import org.gradle.api.tasks.*
import java.io.File

@CacheableTask
internal class Extract : DefaultTask() {
    @get:InputFile
    @get:PathSensitive(PathSensitivity.RELATIVE)
    var archive by GradleProperty(project, "Extract task property 'archive'", File::class.java)

    @get:Input
    var extension by GradleProperty(project, "Extract task property 'extension'", String::class.java)

    @get:OutputDirectory
    var dest by GradleProperty(project, "Extract task property 'dest'", File::class.java)

    @get:Input
    protected var stripPrefix: String? = null

    fun stripPrefix(prefix: String?) {
        stripPrefix = prefix
    }

    @TaskAction
    fun sync() {
        if (dest.exists() && !dest.isDirectory) {
            throw InvalidUserDataException("dest '$dest' has to be a directory")
        }

        val sp = stripPrefix

        project.sync { spec ->
            when (extension) {
                "tar.gz" -> {
                    spec.from(project.tarTree(project.resources.gzip(archive)))
                }
                "zip" -> {
                    spec.from(project.zipTree(archive))
                }
                else -> throw IllegalStateException("Invalid extension")
            }
            if (sp != null) {
                spec.eachFile {
                    it.path = it.path.removePrefix(sp)
                }
            }
            spec.into(dest)
        }
    }
}
