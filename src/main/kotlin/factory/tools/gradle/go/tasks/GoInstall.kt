package factory.tools.gradle.go.tasks

import factory.tools.gradle.go.GradleListProperty
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.CacheableTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.OutputFiles

@CacheableTask
class GoInstall : AbstractGoExec<GoInstall>(GoInstall::class) {
    init {
        group = "go"
    }

    @get:Input
    var modules by GradleListProperty(project, "GoInstall task property 'modules'", String::class.java)

    @OutputFiles
    protected var binaries: Provider<List<String>> = project.provider {
        modules.map {
            val name = it.split('/').last().split('@').first()
            "$gopathBin/$name"
        }
    }

    override fun prepareExec(): List<String> {
        return listOf("$goBin/go", "get") + modules
    }
}
