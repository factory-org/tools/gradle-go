package factory.tools.gradle.go.tasks

import org.slf4j.LoggerFactory

open class GoExec : AbstractGoExec<GoExec>(GoExec::class) {
    private val logger = LoggerFactory.getLogger(GoExec::class.java)

    override fun prepareExec(): List<String> {
        val cmd = commandLine.getOrNull(0)
        val cmdl = commandLine
        if (cmd != null) {
            when {
                project.file("$goBin/$cmd").exists() -> {
                    cmdl[0] = "$goBin/$cmd"
                }
                project.file("$gopathBin/$cmd").exists() -> {
                    cmdl[0] = "$gopathBin/$cmd"
                }
                else -> {
                    logger.warn("Command '$cmd' not found in go sdk or go path")
                }
            }
        }
        return cmdl
    }
}