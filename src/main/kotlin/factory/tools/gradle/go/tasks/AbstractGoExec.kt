package factory.tools.gradle.go.tasks

import factory.tools.gradle.go.golang
import org.gradle.api.file.DirectoryProperty
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.AbstractExecTask
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.TaskAction
import java.io.File
import kotlin.reflect.KClass

abstract class AbstractGoExec<T : AbstractGoExec<T>>(klass: KClass<T>) : AbstractExecTask<T>(klass.java) {
    init {
        @Suppress("LeakingThis")
        dependsOn(project.provider { "installSdk" })
    }

    @get:Internal
    protected val buildDir: DirectoryProperty by lazy {
        if (project.golang.installInRoot) {
            project.rootProject.layout.buildDirectory
        } else {
            project.layout.buildDirectory
        }
    }

    @get:OutputDirectory
    protected val goBin: Provider<File> = buildDir.map { it.file("go/bin").asFile }

    @get:OutputDirectory
    protected val gopathBin: Provider<File> = buildDir.map { it.file("gopath/bin").asFile }

    protected abstract fun prepareExec(): List<String>

    @TaskAction
    override fun exec() {
        prepareEnv()

        commandLine = prepareExec()

        super.exec()
    }

    private fun prepareEnv() {
        environment["GOROOT"] = buildDir.file("go").get().asFile.absolutePath
        environment["GOPATH"] = buildDir.file("gopath").get().asFile.absolutePath
        environment["PATH"] = "${goBin.get().absolutePath}:${gopathBin.get().absolutePath}:${environment["PATH"]}"
    }
}
