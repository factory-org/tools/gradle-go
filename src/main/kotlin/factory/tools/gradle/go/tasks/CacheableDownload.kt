package factory.tools.gradle.go.tasks

import de.undercouch.gradle.tasks.download.Download
import org.gradle.api.tasks.CacheableTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.OutputFile

@CacheableTask
@Suppress("ProtectedMemberInFinalClass")
internal class CacheableDownload : Download() {
    @get:Input
    protected var source: Any? = null

    @get:OutputFile
    protected var outputFile: Any? = null

    override fun src(src: Any) {
        source = src
        super.src(src)
    }

    override fun dest(dest: Any) {
        outputFile = dest
        super.dest(dest)
    }
}
