package factory.tools.gradle.go.tasks

import factory.tools.gradle.go.GoTarget
import factory.tools.gradle.go.GradleNullableProperty
import factory.tools.gradle.go.GradleProperty
import org.gradle.api.*
import org.gradle.api.file.FileCollection
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.CacheableTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputDirectory
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.PathSensitive
import org.gradle.api.tasks.PathSensitivity
import java.io.File

@CacheableTask
@Suppress("ProtectedMemberInFinalClass")
class GoBuild : AbstractGoExec<GoBuild>(GoBuild::class) {
    init {
        group = "build"
    }

    @get:InputFiles
    @get:PathSensitive(PathSensitivity.RELATIVE)
    protected val sources: Provider<FileCollection> = project.provider {
        project.layout.buildDirectory.files("$sourceDir/**/*.go", "$sourceDir/go.mod", "$sourceDir/go.sum")
    }

    @get:OutputFile
    protected val binaryFile: Provider<File> = project.layout.buildDirectory.map { it.file("bin/$binaryName").asFile }

    @get:InputDirectory
    @get:PathSensitive(PathSensitivity.RELATIVE)
    var sourceDir by GradleProperty(
        project,
        "GoBuild task property 'sourceDir'",
        File::class.java,
        project.projectDir
    )

    @get:Input
    var binaryName by GradleProperty(project, "GoBuild task property 'binaryName'", String::class.java, project.name)

    @get:Input
    @get:Optional
    var targetPlatform by GradleNullableProperty(
        project,
        "GoBuild task property 'targetPlatform'",
        GoTarget::class.java
    )

    @get:Input
    @get:Optional
    var cgoEnabled by GradleNullableProperty(project, "GoBuild task property 'cgoEnabled'", Boolean::class.java)

    override fun prepareExec(): List<String> {
        if (!sourceDir.isDirectory) {
            throw InvalidUserDataException("sourceDir '$sourceDir' has to be a directory")
        }

        val tp = targetPlatform
        if (tp != null) {
            environment["GOOS"] = tp.os
            environment["GOARCH"] = tp.arch
        }

        when (cgoEnabled) {
            true -> environment["CGO_ENABLED"] = "1"
            null -> environment["CGO_ENABLED"] = "1"
            false -> environment["CGO_ENABLED"] = "0"
        }

        return listOf(
            "${goBin.get().absolutePath}/go",
            "build",
            "-o",
            binaryFile.get().absolutePath,
        ) + (args ?: listOf())
    }
}
