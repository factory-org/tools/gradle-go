package factory.tools.gradle.go

import io.kotest.core.spec.style.WordSpec
import io.kotest.matchers.shouldNotBe
import org.gradle.testfixtures.ProjectBuilder

class PluginTest : WordSpec({
    "Using the Plugin ID" should {
        "Apply the Plugin" {
            val project = ProjectBuilder.builder().build()
            project.pluginManager.apply("factory.tools.gradle.go")

            project.plugins.getPlugin(GoPlugin::class.java) shouldNotBe null
        }
    }
    "Applying the Plugin" should {
        "Register the 'golang' extension" {
            val project = ProjectBuilder.builder().build()
            project.pluginManager.apply(GoPlugin::class.java)

            project.golang shouldNotBe null
        }
    }
})
