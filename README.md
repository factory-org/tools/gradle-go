# Gradle Go

[![pipeline status](https://gitlab.com/factory-org/tools/gradle-go/badges/master/pipeline.svg)](https://gitlab.com/factory-org/tools/gradle-go/-/commits/master)
[![coverage report](https://gitlab.com/factory-org/tools/gradle-go/badges/master/coverage.svg)](https://gitlab.com/factory-org/tools/gradle-go/-/commits/master)
[![Maven metadata URL](https://img.shields.io/maven-metadata/v?metadataUrl=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fgroups%2F8218378%2F-%2Fpackages%2Fmaven%2Ffactory%2Ftools%2Fgradle%2Fgradle-go%2Fmaven-metadata.xml)](https://gitlab.com/factory-org/tools/gradle-go/-/packages)

A simple gradle plugin for setting up and using go.

## Installation

The plugin can be installed via:

```kotlin
plugins {
    id("com.gitlab.factory.go") version "0.3.2"
}
```

Currently, additional configuration is required, because it is not yet on the
gradle plugin portal:

<details>
<summary>settings.gradle.kts</summary>

```kotlin
pluginManagement {
    repositories {
        gradlePluginPortal()
            maven("https://gitlab.com/api/v4/groups/8218378/-/packages/maven")
    }
}
```

</details>

_Note: The dependencies of the Go SDK have to be installed (e.g. `git`)_

## Configuration

The plugin can be configured via the `go` toplevel extension:

<details>
<summary>build.gradle.kts</summary>

```kotlin
go {
    /**
     * Go SDK version to install.
     */
    version = "1.17"  // required
    /**
     * Whether the SDK and the gopath should reside in the build folder
     * of the root project or in the subproject build folder.
     */
    installInRoot = true  // optional
}
```

</details>

## Usage

### Task Types

#### GoExec

This task type is similar to the `Exec` type but sets the `PATH`, `GOROOT` and `GOPATH` environment variables correctly
and depends on the SDK install.

### Predefined Tasks

The following predefined tasks are available:

#### downloadSdk

Downloads the Go SDK.

_You probably won't need to call this manually, it will be called before the sdk is installed_

#### installSdk

Installs the Go SDK into the build folder.

_If you use [`GoExec`](#goexec) tasks you won't need to install run this task, it will be run before the task is
executed_

#### generateGopathWrapper

Generates a `gopathw` file in the subproject. You can run this file (with `./gopathw`) to get dropped in a bash
shell that has the proper `PATH`, `GOROOT` and `GOPATH` variables set.
